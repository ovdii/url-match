(ns url-match.core
  (:import (java.net URLDecoder))
  (:require [clojure.string :as string]
            [clojure.walk :refer [keywordize-keys]]
            [clojure.test :as t]))

(defn split-param [param]
  (->
   (string/split param #"=")
   (concat (repeat ""))
   (->>
    (take 2))))

(defn query->map
  [qstr]
  (when (not (string/blank? qstr))
    (some->> (string/split qstr #"&")
             seq
             (mapcat split-param)
             (map #(some-> % str (URLDecoder/decode "UTF-8")))
             (apply hash-map))))

(defn make-hash
  [x y]
  (when (keyword? (read-string x))
    (hash-map (read-string x) y)))

;;ugly code, need to rewrite later..
(defn compare-path
  [x y]
  (not (some false? (map #(when (not (keyword? (read-string %1)))
                            (= %1 %2)) x y))))

(defn create-path-params
  [[k p]]
  (if (and (= (count k) (count p))
           (compare-path k p))
    (map #(make-hash %1 %2) k p)))

(defn parse-path
  [& args]
  (->> (map #(string/split % #"/") args)
       create-path-params
       (apply merge)))

(defn parse-string
  "Takes string and returns a lists of key/value"
  [s]
  (->> (string/replace s #"\?" ":")
       (re-seq #"(\w+)\((\S+)\)")
       (map rest)))

(defn convert-to-hashmap
  "Take seqs of key/value and return hash-map"
  [v]
  (->> (map #(hash-map (first %) ;; take until "=" for second arg and make hash
                       (apply str (take-while (fn [x] (not= x \=)) (second %)))) v)
       (keywordize-keys)
       (apply merge-with vector)))

(defn modify-query
  "Return modified version of query value (vectorized)"
  [n]
  (if (instance? String (:queryparam n))
    (assoc n :queryparam (vector (:queryparam n)))
    n))

(defn output
  ([path]
   (->> (into (sorted-map) path)
        (apply vector)))
  ([path query]
   (->> (keywordize-keys query)
        (merge path)
        (into (sorted-map))
        (apply vector))))

(defn new-pattern
  [n]
  (-> (parse-string n)
       convert-to-hashmap
       modify-query))

(defn recognize
  [pattern site]
  (let [link (java.net.URL. site)
        path-matched? (parse-path (:path pattern) (subs (.getPath link) 1))
        host-equal? (= (:host pattern) (.getHost link))
        query-exist? (:queryparam pattern)
        query-link (query->map (.getQuery link))
        params-not-missing? (every? query-link (:queryparam pattern))]
    (if (and host-equal? path-matched?)
      (if query-exist?
        (if params-not-missing?
          (output path-matched?
                  (select-keys query-link (:queryparam pattern))))
        (output path-matched?)))))

;;TESTS

(t/deftest no-query-param
  (t/is (= (recognize
            (new-pattern "host(twitter.com); path(?user/status/?id);")
            "http://twitter.com/bradfitz/status/562360748727611392")
        [[:id "562360748727611392"] [:user "bradfitz"]])))

(t/deftest one-query-params
  (t/is (= (recognize
            (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset);")
            "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1")
           [[:id "1905065-Travel-Icons-pack"] [:offset "1"]])))


(t/deftest multiple-query-params
  (t/is (= (recognize
            (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);")
            "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users&offset=1")
           [[:id "1905065-Travel-Icons-pack"] [:list "users"] [:offset "1"]])))


(t/deftest host-mismatch
  (t/is (= (recognize
            (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);")
            "https://twitter.com/shots/1905065-Travel-Icons-pack?list=users&offset=1")
           nil)))

(t/deftest query-mismatch
  (t/is (= (recognize
            (new-pattern "host(dribbble.com); path(shots/?id); queryparam(offset=?offset); queryparam(list=?type);")
            "https://dribbble.com/shots/1905065-Travel-Icons-pack?list=users")
           nil)))

(t/run-tests 'url-match.core)
